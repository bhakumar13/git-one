package com.sapient.airport.utils;

import com.sapient.airport.model.Airport;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AirportTranslator {

	public static Airport stringToAirport(String airportLine) {
		String stripedLine = airportLine.strip();
		String plainText = stripedLine.replace("\"", "");
		String[] splitData = plainText.split(",");
		Airport airport = new Airport();
		airport.setName(splitData[3]);
		return airport;
	}
}
