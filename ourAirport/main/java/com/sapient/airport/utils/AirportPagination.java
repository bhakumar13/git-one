package com.sapient.airport.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AirportPagination {

	public static <T> List<List<T>> getPages(Collection<T> c, Integer pageSize) {
		if (c == null)
			return Collections.emptyList();
		List<T> list = new ArrayList<>(c);
		if (pageSize == null || pageSize <= 0 || pageSize > list.size())
			pageSize = list.size();
		int numPages = (int) Math.ceil((double) list.size() / (double) pageSize);
		List<List<T>> pages = new ArrayList<>(numPages);
		for (int pageNum = 0; pageNum < numPages; ++pageNum) {
			pages.add(list.subList(pageNum * pageSize, Math.min(pageNum * pageSize, list.size())));
		}
		return pages;
	}

}
