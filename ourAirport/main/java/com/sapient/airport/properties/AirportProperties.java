package com.sapient.airport.properties;

public class AirportProperties {

	private AirportProperties() throws IllegalAccessException {
		throw new IllegalAccessException("Can not access private constructor");
	}

	public static final String DB_FILE = "airport_db_file_path";

}
