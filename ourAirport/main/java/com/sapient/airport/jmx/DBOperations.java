package com.sapient.airport.jmx;

import com.sapient.airport.db.impl.AirportsDatabaseImpl;

public class DBOperations implements IDBOperations {

	public void reloadDatabase() {
		AirportsDatabaseImpl.getInstance().reload();
	}

	@Override
	public void empty() {
		AirportsDatabaseImpl.getInstance().clear();
	}

	public int getRecordCount() {
		return AirportsDatabaseImpl.getInstance().getAirports().size();
	}
}
