package com.sapient.airport.jmx;

public interface IDBOperations {
    void reloadDatabase();
    void empty();
    int getRecordCount();
}
