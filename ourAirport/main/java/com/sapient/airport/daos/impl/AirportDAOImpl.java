package com.sapient.airport.daos.impl;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.airport.constants.AirportSortConstant;
import com.sapient.airport.constants.ApplicationConstant;
import com.sapient.airport.daos.IAirportDAO;
import com.sapient.airport.db.IAirportsDatabase;
import com.sapient.airport.exception.AirportException;
import com.sapient.airport.exception.AirportNotfoundException;
import com.sapient.airport.model.Airport;

/**
 * Class to deal with all the search functionality and sorting related to
 * Airports
 * 
 * @author Umair Ahmed
 * 
 * @see : {@link IAirportDAO}
 *
 */
public class AirportDAOImpl implements IAirportDAO {

	/**
	 * Variable to hold logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(AirportDAOImpl.class);

	/**
	 * Variable to IAirportsDatabase
	 */
	private IAirportsDatabase database;

	/**
	 * Constructor to init IAirportsDatabase
	 * 
	 * @param IAirportsDatabase
	 */
	public AirportDAOImpl(IAirportsDatabase database) {
		super();
		this.database = database;
	}

	/**
	 * 
	 * @param String : name of the airport
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findAirportByName(String airportName, String sortOrder) {
		LOGGER.info("Inside findAirportByName method of AirportDAOImpl class with airportName : {} sortOrder : {} ",
				airportName, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> airportName.equalsIgnoreCase(airport.getName()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getName)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getName).reversed()).collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : Airport type
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findAirportByType(String type, String sortOrder) {
		LOGGER.info("Inside findAirportByType method of AirportDAOImpl class with type : {} sortOrder : {} ", type,
				sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> type.equalsIgnoreCase(airport.getType()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getType)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getType).reversed()).collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : Airport ident
	 * @return List : returns list of airports
	 * @throws AirportNotfoundException : thrown when no airport found for given
	 *                                  ident
	 */
	@Override
	public Airport findByIdent(String ident) {
		LOGGER.info("Inside findByIdent method of class AirportDAOImpl and ident {} ", ident);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> ident.equalsIgnoreCase(aiport.getIdent())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with ident " + ident);
		return airportOptional.get();
	}

	/**
	 * 
	 * @param String : Airport latitude
	 * @return Airport
	 * @throws AirportNotfoundException : thrown when no airport found for given
	 *                                  latitude
	 */
	@Override
	public Airport findByLatitude(String latitude) {
		LOGGER.info("Inside findByLatitude method of class AirportDAOImpl and latitude {} ", latitude);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> latitude.equalsIgnoreCase(aiport.getLatitudeDegrees())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with latitude " + latitude);
		return airportOptional.get();
	}

	/**
	 * 
	 * @param String : Airport longitude
	 * @return Airport
	 * @throws AirportNotfoundException : thrown when no airport found for given
	 *                                  longitude
	 */
	@Override
	public Airport findByLongitude(String longitude) {
		LOGGER.info("Inside findByLongitude method of class AirportDAOImpl and longitude {} ", longitude);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> longitude.equalsIgnoreCase(aiport.getLongitudeDegrees())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with longitude " + longitude);
		return airportOptional.get();
	}

	/**
	 * @param String : gpsCode
	 * @throws AirportNotfoundException : thrown when airport is not found by
	 *                                  GpsCode
	 *
	 */
	@Override
	public Airport findAirportByGpsCode(String gpsCode) {
		LOGGER.info("Inside getAirportByGpsCode method of class AirportDAOImpl and gpsCode {} ", gpsCode);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> gpsCode.equalsIgnoreCase(aiport.getGpsCode())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with gpsCode " + gpsCode);
		return airportOptional.get();
	}

	/**
	 * @param String : iataCode
	 * @throws AirportNotfoundException : thrown when airport is not found by
	 *                                  IataCode
	 *
	 */

	@Override
	public Airport findAirportByIataCode(String iataCode) {
		LOGGER.info("Inside findAirportByIataCode method of class AirportDAOImpl and iataCode {} ", iataCode);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> iataCode.equalsIgnoreCase(aiport.getIataCode())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with iataCode " + iataCode);
		return airportOptional.get();
	}

	/**
	 * @param String : localCode
	 * @throws AirportNotfoundException : thrown when airport is not found by
	 *                                  localCode
	 *
	 */
	@Override
	public Airport findAirportByLocalCode(String localCode) {
		LOGGER.info("Inside findAirportByLocalCode method of class AirportDAOImpl and localCode {} ", localCode);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> localCode.equalsIgnoreCase(aiport.getLocalCode())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with localCode " + localCode);
		return airportOptional.get();

	}

	/**
	 * @param String : homeLink
	 * @throws AirportNotfoundException : thrown when airport is not found by
	 *                                  homeLink
	 *
	 */
	@Override
	public Airport findAirportByHomeLink(String homeLink) {
		LOGGER.info("Inside findAirportByHomeLink method of class AirportDAOImpl and homeLink {} ", homeLink);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> homeLink.equalsIgnoreCase(aiport.getHomeLink())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with homeLink " + homeLink);
		return airportOptional.get();
	}

	/**
	 * @param String : wikipediaLink
	 * @throws AirportNotfoundException : thrown when airport is not found by
	 *                                  wikipediaLink
	 *
	 */
	@Override
	public Airport findAirportByWikipediaLink(String wikipediaLink) {
		LOGGER.info("Inside findAirportByWikipediaLink method of class AirportDAOImpl and WikipediaLink {} ",
				wikipediaLink);
		Optional<Airport> airportOptional = getAiports().get()
				.filter(aiport -> wikipediaLink.equalsIgnoreCase(aiport.getWikipediaLink())).findFirst();
		if (!airportOptional.isPresent())
			throw new AirportNotfoundException("Airport not found with wikipediaLink " + wikipediaLink);
		return airportOptional.get();
	}

	/**
	 * 
	 * @param String : Keywords of the airports
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * 
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findAirportByKeywords(String keyword, String sortOrder) {
		LOGGER.info("Inside findAirportByKeywords method of AirportDAOImpl class with keyword : {} sortOrder : {} ",
				keyword, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> keyword.equalsIgnoreCase(airport.getKeywords()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getKeywords)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getKeywords).reversed()).collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException("Unsupported sort option : " + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : Elevation of the airports
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * 
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findByElevationFeet(String elevationFeet, String sortOrder) {
		LOGGER.info("Inside findByElevationFeet method of AirportDAOImpl class with elevationFeet : {} sortOrder : {} ",
				elevationFeet, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> elevationFeet.equalsIgnoreCase(airport.getElevationFeet()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getElevationFeet)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getElevationFeet).reversed())
					.collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : Continent of the airports
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findByContinent(String continent, String sortOrder) {
		LOGGER.info("Inside findByContinent method of AirportDAOImpl class with continent : {} sortOrder : {} ",
				continent, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> continent.equalsIgnoreCase(airport.getContinent()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getContinent)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getContinent).reversed())
					.collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : IsoCountry of the airports
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findByIsoCountry(String isoCountry, String sortOrder) {
		LOGGER.info("Inside findByIsoCountry method of AirportDAOImpl class with isoCountry : {} sortOrder : {} ",
				isoCountry, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> isoCountry.equalsIgnoreCase(airport.getIsoCountry()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getIsoCountry)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getIsoCountry).reversed())
					.collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : Airport By isoRegion
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findByIsoRegion(String isoRegion, String sortOrder) {
		LOGGER.info("Inside findByIsoRegion method of AirportDAOImpl class with isoRegion : {} sortOrder : {} ",
				isoRegion, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> isoRegion.equalsIgnoreCase(airport.getIsoRegion()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getIsoRegion)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getIsoRegion).reversed())
					.collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : Airport By Municipality
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findByMunicipality(String municipality, String sortOrder) {
		LOGGER.info("Inside findByMunicipality method of AirportDAOImpl class with municipality : {} sortOrder : {} ",
				municipality, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> municipality.equalsIgnoreCase(airport.getMunicipality()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getMunicipality)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getMunicipality).reversed())
					.collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	/**
	 * 
	 * @param String : Airport By scheduled service
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @return List : returns list of airports
	 * @throws AirportException : thrown when unsupported sort option is passed
	 */
	@Override
	public List<Airport> findByScheduledServices(String scheduledService, String sortOrder) {
		LOGGER.info(
				"Inside findByScheduledServices method of AirportDAOImpl class with scheduledService : {} sortOrder : {} ",
				scheduledService, sortOrder);
		final Supplier<Stream<Airport>> sup = () -> getAiports().get()
				.filter(airport -> scheduledService.equalsIgnoreCase(airport.getScheduledService()));
		if (AirportSortConstant.ASE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getScheduledService)).collect(Collectors.toList());
		} else if (AirportSortConstant.DESC.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().sorted(Comparator.comparing(Airport::getScheduledService).reversed())
					.collect(Collectors.toList());
		} else if (AirportSortConstant.NONE.toString().equalsIgnoreCase(sortOrder)) {
			return sup.get().collect(Collectors.toList());
		} else {
			throw new AirportException(ApplicationConstant.UNSUPPORTED_SORT_OPTION + sortOrder);
		}
	}

	private Supplier<Stream<Airport>> getAiports() {
		LOGGER.info("Inside getAiports method of AirportDAOImpl class");
		return this.database.getAirports()::stream;
	}
}
