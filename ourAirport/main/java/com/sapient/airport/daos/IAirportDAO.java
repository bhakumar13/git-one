package com.sapient.airport.daos;

import java.util.List;

import com.sapient.airport.daos.impl.AirportDAOImpl;
import com.sapient.airport.model.Airport;

/**
 * Interface to be implemented by {@link AirportDAOImpl}
 * 
 * @author Umair Ahmed
 *
 */
public interface IAirportDAO {

	/**
	 * Fetch the airports by airport name and apply sorting based on sorting
	 * criteria
	 * 
	 * @param airportName : name of the airport
	 * @param sortOrder   : Sorting criteria
	 * @return List : return the list of airport or empty list
	 */
	List<Airport> findAirportByName(String airportName, String sortOrder);

	/**
	 * Fetch the airports by airport type and apply sorting based on sorting
	 * criteria
	 * 
	 * @param String : Type of the airport
	 * @param String : Sorting criteria
	 * @return List : return the list of airport or empty list
	 */
	List<Airport> findAirportByType(String type, String sortOrder);

	Airport findByIdent(String ident);

	Airport findByLatitude(String latitude);

	Airport findByLongitude(String longitude);
	

	Airport findAirportByGpsCode(String string);

	Airport findAirportByIataCode(String string);

	Airport findAirportByLocalCode(String string);
	

	Airport findAirportByHomeLink(String homeLink);

	Airport findAirportByWikipediaLink(String wikipediaLink);

	List<Airport> findAirportByKeywords(String keywords, String sortOrder);
	

	List<Airport> findByElevationFeet(String elevationFeet, String sortOrder);

	List<Airport> findByContinent(String continent, String sortOrder);

	List<Airport> findByIsoCountry(String isoCountry, String sortOrder);
	

	List<Airport> findByIsoRegion(String isoRegion, String sortOrder);

	List<Airport> findByMunicipality(String municipality, String sortOrder);

	List<Airport> findByScheduledServices(String services, String sortOrder);

}
