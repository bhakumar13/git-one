package com.sapient.airport.launcher;

import java.io.IOException;
import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sapient.airport.jmx.DBOperations;
import com.sapient.airport.jmx.IDBOperations;
import com.sapient.airport.model.Airport;
import com.sapient.airport.properties.AirportProperties;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class Main {

	private static final Logger logger = LoggerFactory.getLogger(Main.class);

	public static void main(String[] args) throws Exception {
		logger.info("Start");
		/* MBean */
		System.setProperty(AirportProperties.DB_FILE,
				"C:\\Users\\umaahmed\\eclipse-workspace\\OurAirPortApp\\src\\main\\resources\\airports.csv");
		ObjectName myMbean = new ObjectName("com.cog.javalearning:type=basic,name=DBOperations");
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		StandardMBean mbean = new StandardMBean(new DBOperations(), IDBOperations.class);
		server.registerMBean(mbean, myMbean);
		logger.info("Started.");

		/* Jetty Server */
		Server jettyServer = new Server();
		ServerConnector connector = new ServerConnector(jettyServer);
		connector.setPort(8082);
		jettyServer.setConnectors(new Connector[] { connector });
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		context.setContextPath("/airports");
		jettyServer.setHandler(context);
		context.addServlet(new ServletHolder(new HelloServlet()), "/*");
		jettyServer.start();
		/** jetty server will start when you launch your main method...... */

	}

	public static class HelloServlet extends HttpServlet {

		private static final long serialVersionUID = -6154475799000019575L;

		@Override
		protected void doGet(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			Airport airport = new Airport();
			airport.setId(1L);
			airport.setName("Lax");
			airport.setContinent("US");
			/**
			 * Maps any pojo and convert to json or from json to java
			 */
			ObjectMapper mapper = new ObjectMapper();
			String airportstring = mapper.writeValueAsString(airport);
			response.setContentType("application/json");
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().println(airportstring);
		}

	}
}
