package com.sapient.airport.validator;

import java.util.function.Predicate;

public enum StringValidator {

	STRING_VALIDATOR(input -> input != null && !input.isBlank()),
	STRING_VALIDATOR_CHECK_LEN_3(input -> input != null && !input.isBlank() && input.length() >= 2),
	STRING_VALIDATOR_CHECK_LEN_2(input -> input != null && !input.isBlank() && input.length() == 2);

	private Predicate<String> predicate;

	StringValidator(Predicate<String> predicate) {
		this.predicate = predicate;
	}

	public boolean test(String input) {
		return predicate.test(input);
	}

}
