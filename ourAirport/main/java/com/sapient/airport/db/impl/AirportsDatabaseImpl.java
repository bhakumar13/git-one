package com.sapient.airport.db.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.airport.db.IAirportsDatabase;
import com.sapient.airport.exception.AirportDataBaseException;
import com.sapient.airport.model.Airport;
import com.sapient.airport.properties.AirportProperties;
import com.sapient.airport.utils.AirportTranslator;

/**
 * @author Umair
 * @since 1.0
 */
public class AirportsDatabaseImpl implements IAirportsDatabase {

	private static final Logger logger = LoggerFactory.getLogger(AirportsDatabaseImpl.class);

	private List<Airport> airports;
	private static final AirportsDatabaseImpl database = new AirportsDatabaseImpl();

	/**
	 * Private Constructor to avoid multiple instance
	 */
	private AirportsDatabaseImpl() {
		List<String> unparsedData = loadDatabaseFile();
		this.airports = parseDatabaseData(unparsedData);
		logger.info("Loaded....");
	}

	/**
	 * Singleton Eager
	 *
	 * @return
	 */
	public static AirportsDatabaseImpl getInstance() {
		return database;
	}

	public List<Airport> getAirports() {
		return airports;
	}

	public void reload() {
		this.airports.clear();
		this.airports = null;
		this.airports = parseDatabaseData(loadDatabaseFile());
	}

	public void clear() {
		this.airports.clear();
	}

	/**
	 * Here is where you load you csv
	 */
	private List<String> loadDatabaseFile() {
		List<String> rawData = null;
		try {
			rawData = Files.readAllLines(Path.of(System.getProperty(AirportProperties.DB_FILE)));
		} catch (Exception e) {
			throw new AirportDataBaseException("No Such File Found");
		}
		return rawData;
	}

	/**
	 * Loaded Data is parsed and Loaded in Desired Format
	 *
	 * @param unparsedData
	 * @return
	 */
	private List<Airport> parseDatabaseData(List<String> unparsedData) {
		return unparsedData.stream().skip(1).map(AirportTranslator::stringToAirport).collect(Collectors.toList());
	}
}
