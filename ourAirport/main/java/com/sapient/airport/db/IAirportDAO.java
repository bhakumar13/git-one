package com.sapient.airport.db;

import java.util.List;

import com.sapient.airport.model.Airport;

public interface IAirportDAO 
{
	 List<Airport> findAllByCode(String code);
	 List<Airport> findAllByName(String name);
	 List<Airport> findAirportByContinent(String continent);
	 
	 List<Airport> findLargeAirportByType(String type);
	 List<Airport> findAirportByCountryIndia(String country);
	 
	 List<Airport> findAirportByLat(String lat);
	 
	 List<Airport> findAirportByLongitude(String longitude);
	 List<Airport> findAirportByAddress(String municipality);
	 List<Airport> findSmallAirportByType(String type);
	 List<Airport> findAirportByElevationFeet(String elevationFeet);
	 List<Airport> findAirportByAirportID(Long airportID); 
}
