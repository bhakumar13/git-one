package com.sapient.airport.db;

import com.sapient.airport.model.Airport;

import java.util.List;

public interface IAirportsDatabase {
	List<Airport> getAirports();

	void reload();

	void clear();

}
