package com.sapient.airport.db;

import com.sapient.airport.model.Airport;
import com.sapient.airport.utils.AirportTranslator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Bhaskar Kumar
 * @since 1.0
 */
public class AirportsDatabase implements IAirportsDatabase {
	
	    private static final String DB_FILE = "db_file";
	    private List<Airport> airports;
	    private static final AirportsDatabase database = new AirportsDatabase();
	    Logger logger = LoggerFactory.getLogger(AirportsDatabase.class);

	    /**
	     * Private Constructor to avoid multiple instance
	     */
	    private AirportsDatabase() {
	        logger.info("Entry::AirportsDatabase()");
	        reloadDB();
	        logger.info("Exit::AirportsDatabase()");
	    }


	    /**
	     * Loaded Data is parsed and Loaded in Desired Format
	     *
	     * @param unparsedData
	     * @return
	     */
	    private List<Airport> parseDatabaseData(List<String> unparsedData) {
	        logger.info("Entry::AirportsDatabase()");
	        List<Airport> airports = unparsedData.stream().skip(1).map(AirportTranslator::stringToAirport).collect(Collectors.toList());
	        logger.info("Exit::AirportsDatabase()");
	        return airports;
	    }

	    /**
	     * Here is where you load you csv
	     */
	    private List<String> loadDatabaseFile() throws RuntimeException {
	        logger.info("Entry::loadDatabaseFile()");
	        List<String> rawData = null;
	        try {
	            rawData = Files.readAllLines(Path.of(System.getProperty(DB_FILE)));
	        } catch (Exception e) {
	            // TODO Refactor for better exception handling
	            throw new RuntimeException("No Such File Found");
	        }
	        logger.info("Exit::loadDatabaseFile()");
	        return rawData;
	    }

	    /**
	     * Singleton Eager
	     *
	     * @return
	     */
	    public static AirportsDatabase getInstance() {
	        return database;
	    }

	    @Override
	    public List<Airport> getAirports() {
	        logger.info("Exit::getAirports()");
	        return airports;
	    }


	    public void reloadDB() {
	        logger.info("Enter::reloadDB()");
	        List<String> unparsedData = loadDatabaseFile();
	        this.airports = parseDatabaseData(unparsedData);
	        logger.info("Enter::reloadDB()");
	    }

	    public void empty() {
	        logger.info("Enter::empty()");
	        this.airports.clear();
	        logger.info("Exit::empty()");
	    }

}
