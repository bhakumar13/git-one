package com.sapient.airport.constants;

public enum AirportSortConstant {

	NONE, ASE, DESC;
}
