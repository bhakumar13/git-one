package com.sapient.airport.services;

import java.util.List;

import com.sapient.airport.model.Airport;

public interface DummyAirportService {
	List<Airport> findAirpotByCode(String code);
	
	List<Airport> findAirportByName(String name);
	List<Airport> findAirportByContinent(String continent);
	List<Airport> findLargeAirportByType(String type);
	List<Airport> findAirportByCountryIndia(String continent);
	List<Airport> findAirportByLatitude(String lat);
	
	List<Airport> findAirportByLongitude(String longitude);
	 List<Airport> findAirportByAddress(String municipality);
	 List<Airport> findSmallAirportByType(String type);
	 List<Airport> findAirportByElevationFeet(String elevationFeet);
	 List<Airport> findAirportByAirportId(Long airportID);
}
