package com.sapient.airport.services;

public class AirportFactory {

	private static final DummyAirportService dummy = null;

	/**
	 * Singleton implementations to return eager implementation of the Airport
	 */
	public static DummyAirportService getInstance() {
		return dummy;
	}

}
