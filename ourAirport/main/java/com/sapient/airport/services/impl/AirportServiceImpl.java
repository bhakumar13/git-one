package com.sapient.airport.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sapient.airport.daos.IAirportDAO;
import com.sapient.airport.exception.AirportBadParameterException;
import com.sapient.airport.model.Airport;
import com.sapient.airport.services.IAirportService;
import com.sapient.airport.validator.StringValidator;

public class AirportServiceImpl implements IAirportService {
	private static final Logger logger = LoggerFactory.getLogger(AirportServiceImpl.class);

	private IAirportDAO airportDAO;

	public AirportServiceImpl(IAirportDAO airportDAO) {
		super();
		this.airportDAO = airportDAO;
	}

	public List<Airport> getAirportByName(String aiportName, String sortOrder) {
		logger.info("Inside getAirportByName method of class AirportService and airportName {}  sortOrder {} ",
				aiportName, sortOrder);
		if (!StringValidator.STRING_VALIDATOR.test(aiportName)) {
			throw new AirportBadParameterException("Airport name it should not be null or empty");
		}
		return airportDAO.findAirportByName(aiportName, sortOrder);
	}

	@Override
	public Airport getAirportByGpsCode(String gpsCode) {
		logger.info("Inside getAirportByGpsCode method of class AirportService and gpsCode {}", gpsCode);
		if (!StringValidator.STRING_VALIDATOR_CHECK_LEN_3.test(gpsCode)) {
			throw new AirportBadParameterException(
					"gpsCode should not be null or empty or length should not less then 3");
		}
		return airportDAO.findAirportByGpsCode(gpsCode);

	}

	@Override
	public Airport getAirportByIataCode(String iataCode) {
		logger.info("Inside getAirportByIataCode method of class AirportService and iataCode {}", iataCode);
		if (!StringValidator.STRING_VALIDATOR_CHECK_LEN_3.test(iataCode)) {
			throw new AirportBadParameterException(
					"iataCode should not be null or empty or length should not less then 3");
		}
		return airportDAO.findAirportByIataCode(iataCode);
	}

	@Override
	public Airport getAirportByLocalCode(String localCode) {
		logger.info("Inside getAirportByLocalCode method of class AirportService and localCode {}", localCode);
		if (!StringValidator.STRING_VALIDATOR_CHECK_LEN_3.test(localCode)) {
			throw new AirportBadParameterException(
					"localCode should not be null or empty or length should not less then 3");
		}

		return airportDAO.findAirportByLocalCode(localCode);
	}

	/**
	 * @param String : homeLink
	 * @throws AirportBadParameterException : thrown when homeLink is null or empty
	 *
	 */
	@Override
	public Airport getAirportByHomeLink(String homeLink) {
		logger.info("Inside getAirportByHomeLink method of class AirportService and homeLink {}", homeLink);
		if (!StringValidator.STRING_VALIDATOR.test(homeLink)) {
			throw new AirportBadParameterException("Airport name it should not be null or empty");
		}
		return airportDAO.findAirportByHomeLink(homeLink);
	}

	/**
	 * @param String : wikipediaLink
	 * @throws AirportBadParameterException : thrown when wikipediaLink is null or
	 *                                      empty
	 *
	 */
	@Override
	public Airport getAirportByWikipediaLink(String wikipediaLink) {
		logger.info("Inside getAirportByWikipediaLink method of class AirportService and wikipediaLink {}",
				wikipediaLink);
		if (!StringValidator.STRING_VALIDATOR.test(wikipediaLink)) {
			throw new AirportBadParameterException("Airport name it should not be null or empty");
		}
		return airportDAO.findAirportByWikipediaLink(wikipediaLink);
	}

	/**
	 * @param String : keyword
	 * @param String : sorting option e.i ASE,DESC or NONE
	 * @throws AirportBadParameterException : thrown when keyword is null or empty
	 *
	 */
	@Override
	public List<Airport> getAirportByKeyword(String keyword, String sortOrder) {
		logger.info("Inside getAirportByKeyword method of class AirportService and keyword {}  sortOrder {} ", keyword,
				sortOrder);
		if (!StringValidator.STRING_VALIDATOR.test(keyword)) {
			throw new AirportBadParameterException("Airport keyword should not be null or empty");
		}
		return airportDAO.findAirportByKeywords(keyword, sortOrder);
	}

	@Override
	public List<Airport> getAirportByContinent(String continent, String sortCriteria) {
		logger.info("Inside getAirportByContinent method of class AirportService and continent {}  sortOrder {} ",
				continent, sortCriteria);
		if (!StringValidator.STRING_VALIDATOR_CHECK_LEN_2.test(continent)) {
			throw new AirportBadParameterException(
					"Continent it should not be null or empty and Lenght of continent should be  exactly of  2 length");
		}
		return airportDAO.findByContinent(continent, sortCriteria);
	}

	@Override
	public List<Airport> getAirportByIsoCountry(String isoCountry, String sortCriteria) {
		logger.info("Inside getAirportByIsoCountry method of class AirportService and Iso Country {}  sortOrder {} ",
				isoCountry, sortCriteria);
		if (!StringValidator.STRING_VALIDATOR_CHECK_LEN_2.test(isoCountry)) {
			throw new AirportBadParameterException(
					"Country it should not be null or empty and Lenght of country should be  exactly of  2 length");
		}
		return airportDAO.findByIsoCountry(isoCountry, sortCriteria);
	}

	@Override
	public List<Airport> getAirportByElevationFeet(String elevationFeet, String sortCriteria) {
		logger.info(
				"Inside getAirportByElevationFeet method of class AirportService and Elevation Feet {}  sortOrder {} ",
				elevationFeet, sortCriteria);
		if (!StringValidator.STRING_VALIDATOR.test(elevationFeet)) {
			throw new AirportBadParameterException("Elevation Feet should not be null or empty");
		}
		return airportDAO.findByElevationFeet(elevationFeet, sortCriteria);
	}

	@Override
	public List<Airport> getAirportByIsoRegion(String isoRegion, String sortCriteria) {

		logger.info("Inside getAirportByIsoRegion method of class AirportService and isoRegion {} ", isoRegion);
		if (isoRegion == null || isoRegion.isBlank()) {
			throw new AirportBadParameterException("Bad paramter ,Airport isoRegion it should not be null or empty");
		}

		return airportDAO.findByIsoRegion(isoRegion, sortCriteria);
	}

	@Override
	public List<Airport> getAirportByMunicipality(String municipality, String sortCriteria) {

		logger.info("Inside getAirportByMunicipality method of class AirportService and municipality {} ",
				municipality);
		if (municipality == null || municipality.isBlank()) {
			throw new AirportBadParameterException("Bad paramter ,Airport municipality it should not be null or empty");
		}

		return airportDAO.findByMunicipality(municipality, sortCriteria);
	}

	

}
