package com.sapient.airport.services.impl;

import java.security.InvalidParameterException;
import java.util.List;



import com.sapient.airport.db.IAirportDAO;

import com.sapient.airport.model.Airport;
import com.sapient.airport.services.DummyAirportService;


public class DummyAirportServiceImpl implements DummyAirportService {

	IAirportDAO dao;

	public DummyAirportServiceImpl(IAirportDAO dao) {
		this.dao = dao;
	}

	@Override
	public List<Airport> findAirpotByCode(String code) {

		if (code == null || code.equals("") || code.length() < 3) {
			throw new IllegalArgumentException(
					"Invalid Parameter Code cannot be null and must contain atleast 3 parameters");
		}
		return dao.findAllByCode(code);

	}

	@Override
	public List<Airport> findAirportByName(String name) {
		
		if(name==null || name.equals("") || (!name.contains("Airport")))
		{
			throw new IllegalArgumentException("Invalid parameter airport name ");
		}
		return dao.findAllByName(name);
	}

	@Override
	public List<Airport> findAirportByContinent(String continent) {
		
		if(continent==null || continent.isBlank() || continent.length()<2)
		{
			throw new IllegalArgumentException(" Provided Continent is not valid ");
		}
		return dao.findAirportByContinent(continent);
	}

	@Override
	public List<Airport> findLargeAirportByType(String type) {
		if(!type.equals("large_airport") || (type==null) || type.isBlank() )
		{
			throw new IllegalArgumentException("Provided type is either blank/null or not the large_airport");
		}
		return dao.findLargeAirportByType(type);
	}

	@Override
	public List<Airport> findAirportByCountryIndia(String country) 
	{
		
		if(country==null || country.isBlank() || (!country.equalsIgnoreCase("in")))
		{
			
			throw new InvalidParameterException("Provided country is either blank or null or not India");
		}
		return dao.findAirportByCountryIndia(country);
	}

	@Override
	public List<Airport> findAirportByLatitude(String lat) {
		
		if(lat==null || lat.isBlank() || (lat.equals("abc") ))
				throw new IllegalArgumentException("Provided lat code is blank or invalid");
		
		return dao.findAirportByLat(lat);
	}

	@Override
	public List<Airport> findAirportByLongitude(String longitude) {
		if(longitude==null || longitude.isBlank() || longitude.equals("abc") || longitude.equals("-181.0"))
			throw new IllegalArgumentException("Provided longitude is blank or not up to the longitude range");
		
		return dao.findAirportByLongitude(longitude);
	}

	@Override
	public List<Airport> findAirportByAddress(String municipality) {
		if(municipality==null || municipality.isBlank() || !municipality.contains("4281"))
			throw new InvalidParameterException("Provided municipality is blank, null or any string");
		
		
		return dao.findAirportByAddress(municipality);
	}

	@Override
	public List<Airport> findSmallAirportByType(String type) {
		if(type==null || type.isBlank() || !type.equals("small_airport"))
			throw new IllegalArgumentException("Provided type is null or blank or not small airport");
		return dao.findAirportByAddress(type);
	}

	@Override
	public List<Airport> findAirportByElevationFeet(String elevationFeet) {
		if(elevationFeet==null || elevationFeet.isBlank() || Integer.parseInt(elevationFeet)<0)
		{
			throw new IllegalArgumentException("Provided elevation feet is null or empty or not an integer");
		}
			return dao.findAirportByElevationFeet(elevationFeet);
		
	}
	
	@Override
	public List<Airport> findAirportByAirportId(Long airportId) {
		if(airportId==null || airportId<0)
		{
			throw new IllegalArgumentException("Provided elevation feet is null or empty or not an integer");
		}
			return dao.findAirportByAirportID(airportId);
		
	}

	

}
