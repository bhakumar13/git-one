package com.sapient.airport.services;

import java.util.List;

import com.sapient.airport.model.Airport;

/**
 * 
 * @author Umair
 *
 */
public interface IAirportService {

	List<Airport> getAirportByName(String airportName, String sortCriteria);

	
	Airport getAirportByGpsCode(String gpsCode);

	Airport getAirportByIataCode(String iataCode);

	Airport getAirportByLocalCode(String localCode);
	

	List<Airport> getAirportByKeyword(String aiportName, String sortOrder);

	Airport getAirportByHomeLink(String homeLink);

	Airport getAirportByWikipediaLink(String wikipediaLink);
	

	List<Airport> getAirportByContinent(String continent, String sortCriteria);

	List<Airport> getAirportByIsoCountry(String isoCountry, String sortCriteria);

	List<Airport> getAirportByElevationFeet(String elevationFeet, String sortCriteria);
	

	List<Airport> getAirportByIsoRegion(String isoRegion, String sortCriteria);

	List<Airport> getAirportByMunicipality(String municipality, String sortCriteria);

	
}
