package com.sapient.airport.model;

import lombok.Data;

/**
 * @Author Umair
 * @date 24-09-2021 Standard pojo for Data Structure ...
 */

@Data
public class Airport {
	private Long id;
	private String ident;
	private String type; // by name 
	private String name; // by type 
	private String latitudeDegrees; // by lat 
	private String longitudeDegrees; // by lon
	private String elevationFeet;
	private String continent; 
	private String isoCountry; 
	private String isoRegion; 
	private String municipality;
	private String scheduledService;
	private String gpsCode;
	private String iataCode;
	private String localCode;
	private String homeLink;
	private String wikipediaLink;
	private String keywords;

}
