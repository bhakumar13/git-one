package com.sapient.airport.exception;

/**
 * Exception class to be used when search for airport returns nothing
 * 
 * @author Umair
 *
 */
public class AirportBadParameterException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AirportBadParameterException(String message) {
		super(message);
	}

}
