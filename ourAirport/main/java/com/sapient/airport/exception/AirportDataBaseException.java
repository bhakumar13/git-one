package com.sapient.airport.exception;

/**
 * Exception class to be used when search for airport returns nothing
 * 
 * @author Umair
 *
 */
public class AirportDataBaseException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AirportDataBaseException(String message) {
		super(message);
	}
}
