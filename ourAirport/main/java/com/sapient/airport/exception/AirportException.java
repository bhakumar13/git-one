package com.sapient.airport.exception;

/**
 * Exception class to be used when search for airport returns nothing
 * 
 * @author Umair
 *
 */
public class AirportException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AirportException(String message) {
		super(message);
	}

}
