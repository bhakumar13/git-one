package com.airports.daos;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.sapient.airport.constants.AirportSortConstant;
import com.sapient.airport.daos.IAirportDAO;
import com.sapient.airport.daos.impl.AirportDAOImpl;
import com.sapient.airport.db.IAirportsDatabase;
import com.sapient.airport.exception.AirportException;
import com.sapient.airport.exception.AirportNotfoundException;
import com.sapient.airport.model.Airport;

@DisplayName("Airport DAO Test Scenarios")
class AirportDAOTest {

	private IAirportDAO airportsDAO;

	private IAirportsDatabase database;

	private Airport airport;
	private List<Airport> airports;

	@BeforeEach
	public void init() {
		database = Mockito.mock(IAirportsDatabase.class);
		airportsDAO = new AirportDAOImpl(database);
		airport = new Airport();
		airport.setName("Dubai");
		airport.setType("heliport");
		airport.setIdent("00A");
		airport.setLatitudeDegrees("40.07080078125");
		airport.setLongitudeDegrees("-74.9336013793945");
		airport.setGpsCode("KLAX");
		airport.setIataCode("LAX");
		airport.setLocalCode("LAX");
		airport.setHomeLink("http://www.wsdot.wa.gov/aviation/AllStateAirports/Colfax_LowerGraniteState.htm");
		airport.setWikipediaLink("https://en.wikipedia.org/wiki/McKenzie_Bridge_State_Airport");
		airport.setKeywords("00AR");
		airport.setContinent("Asia");
		airport.setElevationFeet("100");
		airport.setIsoCountry("UAE");
		airport.setIsoRegion("US-PA");
		airport.setMunicipality("Bensalem");
		airport.setScheduledService("no");
		airports = new ArrayList<>();
		airports.add(airport);
		when(this.database.getAirports()).thenReturn(airports);
	}

	@DisplayName("Should returns list of aiprorts when searched by airport type")
	@Test
	void testFindAirportByType() {
		assertNotNull(airportsDAO.findAirportByType("heliport", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findAirportByType("heliport", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by airport type asc order")
	@Test
	void testFindAirportByTypeASC() {
		assertNotNull(airportsDAO.findAirportByType("heliport", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findAirportByType("heliport", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by airport type desc orer")
	@Test
	void testFindAirportByTypeDESC() {
		assertNotNull(airportsDAO.findAirportByType("heliport", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findAirportByType("heliport", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport type and no aiport found by airport type")
	@Test
	void testFindAirportByTypeNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findAirportByType("heliport", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByTypeBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findAirportByType("heliport", "X"));
	}

	@DisplayName("Should returns list of aiprorts when searched by airport name")
	@Test
	void testFindAirportByName() {
		assertNotNull(airportsDAO.findAirportByName("Dubai", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findAirportByName("Dubai", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by airport name asc order")
	@Test
	void testFindAirportByNameASC() {
		assertNotNull(airportsDAO.findAirportByName("Dubai", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findAirportByName("Dubai", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by airport name desc orer")
	@Test
	void testFindAirportByNameDESC() {
		assertNotNull(airportsDAO.findAirportByName("Dubai", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findAirportByName("Dubai", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport name and no aiport found by airport name")
	@Test
	void testFindAirportByNameNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findAirportByName("Dubai", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByNameBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findAirportByName("Dubai", "X"));
	}

	@DisplayName("Should returns an aiprorts when searched by ident")
	@Test
	void testFindByIdent() {
		assertNotNull(airportsDAO.findByIdent("00A"));
	}

	@DisplayName("Should throw an aiport not found exception when no airport found for given ident ")
	@Test
	void testFindByIdentNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO.findByIdent("00AA"));
	}

	@DisplayName("Should returns an aiprorts when searched by ident")
	@Test
	void testFindByLatitude() {
		assertNotNull(airportsDAO.findByLatitude("40.07080078125"));
	}

	@DisplayName("Should throw an aiport not found exception when no airport found for given latutude ")
	@Test
	void testFindByLatitudeNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO.findByLatitude("40.070800"));
	}

	@DisplayName("Should returns an aiprorts when searched by longitude")
	@Test
	void testFindByLongitude() {
		assertNotNull(airportsDAO.findByLongitude("-74.9336013793945"));
	}

	@DisplayName("Should throw an aiport not found exception when no airport found for given longitude ")
	@Test
	void testFindByLongitudeeNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO.findByLongitude("-74.9336"));
	}

	@DisplayName("Should returns aiprort when searched by gpsCode")
	@Test
	void testFindAirportByGpsCode() {
		assertNotNull(airportsDAO.findAirportByGpsCode("KLAX"));
	}

	@DisplayName("Should throw an aiport not found exception when airport is not found with the given gps code ")
	@Test
	void testFindAirportByGpsCodeNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO.findAirportByGpsCode("VILK"));

	}

	@DisplayName("Should returns aiprort when searched by IataCode")
	@Test
	void testFindAirportByIataCode() {
		assertNotNull(airportsDAO.findAirportByIataCode("LAX"));

	}

	@DisplayName("Should throw an aiport not found exception when airport is not found with the given Iata Code ")
	@Test
	void testFindAirportByIataCodeNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO.findAirportByIataCode("LKO"));

	}

	@DisplayName("Should returns aiprort when searched by LocalCode")
	@Test
	void testFindAirportByLocalCode() {
		assertNotNull(airportsDAO.findAirportByLocalCode("LAX"));

	}

	@DisplayName("Should throw an aiport not found exception when airport is not found with the given LocalCode ")
	@Test
	void testFindByLocalCodeNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO.findAirportByLocalCode("0A"));
	}

	@DisplayName("Should Return Airport Details When Searched By HomeLink ")
	@Test
	void testFindAirportByHomeLink() {
		assertNotNull(airportsDAO.findAirportByHomeLink(
				"http://www.wsdot.wa.gov/aviation/AllStateAirports/Colfax_LowerGraniteState.htm"));
	}

	@DisplayName("Should throw an airport not found exception  when serached by airport home link")
	@Test
	void testFindAirportByHomeLinkNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO.findAirportByHomeLink(
				"http://www.wsdot.wa.gov/aviation/AllStateAirports/Colfax_LowerGraniteStaate.ht"));
	}

	@DisplayName("Should Return Airport Details When Searched By wikipediaLink ")
	@Test
	void testFindAirportByWikipediaLink() {
		assertNotNull(
				airportsDAO.findAirportByWikipediaLink("https://en.wikipedia.org/wiki/McKenzie_Bridge_State_Airport"));
	}

	@DisplayName("Should throw an airport not found exception  when serached by airport wiki link ")
	@Test
	void testFindAirportByWikipediaLinkNotFound() {
		assertThrows(AirportNotfoundException.class, () -> airportsDAO
				.findAirportByWikipediaLink("https://en.wikipedia.org/wiki/McKenzie_Bridge_State1_Airport.htm"));
	}

	@DisplayName("Should returns list of aiprorts when searched by airport type")
	@Test
	void testFindAirportByKeword() {
		assertNotNull(airportsDAO.findAirportByKeywords("00AR", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findAirportByKeywords("00AR", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by airport type asc order")
	@Test
	void testFindAirportByKewordASC() {
		assertNotNull(airportsDAO.findAirportByKeywords("00AR", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findAirportByKeywords("00AR", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by airport type desc orer")
	@Test
	void testFindAirportByKewordDESC() {
		assertNotNull(airportsDAO.findAirportByKeywords("00AR", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findAirportByKeywords("00AR", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport type and no aiport found by airport type")
	@Test
	void testFindAirportByKewordNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findAirportByKeywords("00AR0", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByKewordBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findAirportByKeywords("00AR0", "X"));
	}

	@DisplayName("Should returns list of aiprorts when searched by Elevation Feet")
	@Test
	void testFindAirportByElevationFeet() {
		assertNotNull(airportsDAO.findByElevationFeet("100", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findByElevationFeet("100", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Elevation Feet asc order")
	@Test
	void testFindAirportByElevationFeetASC() {
		assertNotNull(airportsDAO.findByElevationFeet("100", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findByElevationFeet("100", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Elevation Feet desc orer")
	@Test
	void testFindAirportByElevationFeetDESC() {
		assertNotNull(airportsDAO.findByElevationFeet("100", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findByElevationFeet("100", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport Elevation Feet and no aiport found by Elevation Feet")
	@Test
	void testFindAirportByElevationFeetNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findByElevationFeet("100", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByElevationFeetBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findByElevationFeet("100", "X"));
	}

	@DisplayName("Should returns list of aiprorts when searched by Elevation Feet")
	@Test
	void testFindAirportByContinent() {
		assertNotNull(airportsDAO.findByContinent("Asia", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findByContinent("Asia", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Elevation Feet asc order")
	@Test
	void testFindAirportByContinentASC() {
		assertNotNull(airportsDAO.findByContinent("Asia", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findByContinent("Asia", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Elevation Feet desc orer")
	@Test
	void testFindAirportByContinentDESC() {
		assertNotNull(airportsDAO.findByContinent("Asia", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findByContinent("Asia", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport Elevation Feet and no aiport found by Elevation Feet")
	@Test
	void testFindAirportByContinentNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findByContinent("Asia", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByContinentBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findByContinent("Asia", "X"));
	}

	@DisplayName("Should returns list of aiprorts when searched by Iso Country")
	@Test
	void testFindAirportByIsoCountry() {
		assertNotNull(airportsDAO.findByIsoCountry("UAE", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findByIsoCountry("UAE", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Iso Country asc order")
	@Test
	void testFindAirportByIsoCountryASC() {
		assertNotNull(airportsDAO.findByIsoCountry("UAE", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findByIsoCountry("UAE", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Iso Country desc orer")
	@Test
	void testFindAirportByIsoCountryDESC() {
		assertNotNull(airportsDAO.findByIsoCountry("UAE", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findByIsoCountry("UAE", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport Iso Country and no aiport found by Iso Country")
	@Test
	void testFindAirportByIsoCountryNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findByIsoCountry("UAE", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByIsoCountryBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findByIsoCountry("UAE", "X"));
	}

	@DisplayName("Should returns list of aiprorts when searched by Iso Region")
	@Test
	void testFindAirportByIsoRegion() {
		assertNotNull(airportsDAO.findByIsoRegion("US-PA", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findByIsoRegion("US-PA", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Iso Region asc order")
	@Test
	void testFindAirportByIsoRegionASC() {
		assertNotNull(airportsDAO.findByIsoRegion("US-PA", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findByIsoRegion("US-PA", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Iso region desc orer")
	@Test
	void testFindAirportByIsoRegionDESC() {
		assertNotNull(airportsDAO.findByIsoRegion("US-PA", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findByIsoRegion("US-PA", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport Elevation Feet and no aiport found by Elevation Feet")
	@Test
	void testFindAirportByIsoRegionNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findByIsoRegion("US-PA", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByIsoRegionsBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findByIsoRegion("US-PA", "X"));
	}

	@DisplayName("Should returns list of aiprorts when searched by Municipality")
	@Test
	void testFindAirportByMunicipality() {
		assertNotNull(airportsDAO.findByMunicipality("Bensalem", AirportSortConstant.NONE.toString()));
		assertFalse(airportsDAO.findByMunicipality("Bensalem", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Municipality asc order")
	@Test
	void testFindAirportByMunicipalityASC() {
		assertNotNull(airportsDAO.findByMunicipality("Bensalem", AirportSortConstant.ASE.toString()));
		assertFalse(airportsDAO.findByMunicipality("Bensalem", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of aiprorts when searched by Municipality desc orer")
	@Test
	void testFindAirportByMunicipalityDESC() {
		assertNotNull(airportsDAO.findByMunicipality("Bensalem", AirportSortConstant.DESC.toString()));
		assertFalse(airportsDAO.findByMunicipality("Bensalem", AirportSortConstant.DESC.toString()).isEmpty());
	}

	@DisplayName("Should return empty airport list when searched by airport Elevation Feet and no aiport found by Elevation Feet")
	@Test
	void testFindAirportByMunicipalityNotFound() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertTrue(airportsDAO.findByMunicipality("Bensalem", AirportSortConstant.NONE.toString()).isEmpty());
	}

	@DisplayName("Should throw an aiport exception when invalid sort option is given other then support ")
	@Test
	void testFindAirportByMunicipalityBadParamter() {
		when(database.getAirports()).thenReturn(Collections.emptyList());
		assertThrows(AirportException.class, () -> airportsDAO.findByMunicipality("Bensalem", "X"));
	}

}
