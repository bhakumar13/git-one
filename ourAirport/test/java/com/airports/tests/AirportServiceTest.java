package com.airports.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.InvalidClassException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.sapient.airport.db.IAirportDAO;
import com.sapient.airport.model.Airport;
import com.sapient.airport.services.DummyAirportService;
import com.sapient.airport.services.impl.DummyAirportServiceImpl;
//import com.sun.media.sound.InvalidDataException;

@DisplayName("Collection of all Finder Scenarios")
public class AirportServiceTest {

	// UnderTest Interface
	DummyAirportService dummy = null;
	// Dependency
	IAirportDAO dao;
	List<Airport> list;
	@BeforeEach
	public void initTest() {
		// underTest Class
		dao = Mockito.mock(IAirportDAO.class);
		dummy = new DummyAirportServiceImpl(dao);
		list = new ArrayList();
		Airport airport = new Airport();
		Airport airport2=new Airport();
		airport.setGpsCode("LAX");
		airport.setName("Los Angeles International Airpirt");
		airport.setName("AF");
		airport.setType("large_airport");
		airport.setIsoCountry("IN");
		airport.setLatitudeDegrees("44.01");
		airport.setLongitudeDegrees("-74.93");
		airport.setMunicipality("4281");
		airport2.setType("small_airport");
		airport.setElevationFeet("11");
		airport.setId((long) 6523);
		list.add(airport);
		list.add(airport2);
		
		Mockito.when(dao.findAllByCode("LAX")).thenReturn(list);
		Mockito.when(dao.findAllByName("Los Angeles International Airport")).thenReturn(list);
		Mockito.when(dao.findAirportByContinent("AF")).thenReturn(list);
		Mockito.when(dao.findLargeAirportByType("large_airport")).thenReturn(list);
		Mockito.when(dao.findAirportByLongitude("-74.93")).thenReturn(list);
		Mockito.when(dao.findAirportByAddress("4281")).thenReturn(list);
		Mockito.when(dao.findSmallAirportByType("small_airport")).thenReturn(list);
		Mockito.when(dao.findAirportByElevationFeet("11")).thenReturn(list);
		Mockito.when(dao.findAirportByAirportID((long) 6523)).thenReturn(list);
		
	}

	@DisplayName("Should return List of Airports when a airport Code is passed")
	@Test
	void testFindAirportByCode() {
		assertFalse(dummy.findAirpotByCode("LAX").isEmpty());
	}

	@DisplayName("Should Throw IllegalArgumentException when airport code is blank/empty or null")
	@Test
	void testFindAirportByCodeBadParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when Airport Code less than 3 chars")
	@Test
	void testFindAirportByCodeBadParameterLength() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("A");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("AB");
		});
	}
	
	@DisplayName("Should return A list of Airports when Airport Name is passed")
	@Test
	void testFindAirportByName() {
		assertTrue(dummy.findAirportByName("Los Angeles International Airport").size()>0);
	}

	@DisplayName("Should Throw IllegalArgumentException when airport name provided  is blank/empty or null")
	@Test
	void testFindAirportByNameBadParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByName(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByName("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when Airport continent does not contain Airport")
	@Test
	void testFindAirportByNameBadParameterLength() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByName("Los Angeles International");
		});
//		assertThrows(IllegalArgumentException.class, () -> {
//			dummy.findAirpotByCode("AB");
//		});
	}
	
	@DisplayName("Should return List of Airports when a airport continent is passed")
	@Test
	void testFindAirportByContinent() {
		assertFalse(dummy.findAirportByContinent("AF").isEmpty());
	}

	@DisplayName("Should Throw IllegalArgumentException when airport continent is blank/empty or null")
	@Test
	void testFindAirportByContinentBadParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByContinent(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByContinent("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when Airport Continent less than 2 chars")
	@Test
	void testFindAirportByContinentBadParameterLength() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("A");
		});
		
	}
	
	@DisplayName("Should return List of Airports when a large airport is searched")
	@Test
	void testLargeAirportByType() {
		assertFalse(dummy.findLargeAirportByType("large_airport").isEmpty());
	}

//	@DisplayName("Should Throw IllegalArgumentException when airport searched By Large Airport is blank/empty or null")
//	@Test
//	void testLargeAirportByTypeBadParameters() {
//		assertThrows(IllegalArgumentException.class, () -> {
//			dummy.findLargeAirportByType(null);
//		});
//		assertThrows(IllegalArgumentException.class, () -> {
//			dummy.findLargeAirportByType("");
//		});
//
//	}

	@DisplayName("Should throw IllegalArgumentException when Airport By Type does not contain large airports")
	@Test
	void testLargeAirportByDifferentTypeBadParameters() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findLargeAirportByType("small_airport");
		});
		
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findLargeAirportByType("medium_airport");
		});
		
	}
	

	@DisplayName("Should return List of Airports when a country is passed")
	@Test
	void testFindAirportByCountryIndia() {
		
		Mockito.when(dao.findAirportByCountryIndia("IN")).thenReturn(list);
		assertTrue(dummy.findAirportByCountryIndia("IN").size()>0);
	}

	@DisplayName("Should Throw IllegalArgumentException when provided country  is blank/empty or null")
	@Test
	void testFindAirportByCountryIndiaBadParameter() {
		assertThrows(InvalidParameterException.class, () -> {
			dummy.findAirportByCountryIndia(null);
		});
		assertThrows(InvalidParameterException.class, () -> {
			dummy.findAirportByCountryIndia("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException if provided is not India")
	@Test
	void testFindAirportCheckCountryIndia(){
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("US");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("As");
			
		});
		
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("AF");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirpotByCode("AG");
		});
	}
	
	@DisplayName("Should return A list of Airports when a latitude is passed")
	@Test
	void testFindAirportByLat() {
		Mockito.when(dao.findAirportByLat("44.01")).thenReturn(list);
		assertTrue(dummy.findAirportByLatitude("44.01").size()>0);
	}

	@DisplayName("Should Throw IllegalArgumentException when latitude  provided  is blank/empty or null")
	@Test
	void testFindAirportByLatBadParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByCountryIndia(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByLatitude("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when latitude does not have valid entry")
	@Test
	void testFindAirportByLatBadParameterInDegrees() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByLatitude("abc");
		});
//		assertThrows(InvalidParameterException.class, () -> {
//			dummy.findAirportByLatitude("-91.0");
//		});
	}
	
	
	@DisplayName("Should return A list of Airports when a longitude is passed")
	@Test
	void testFindAirportByLongitude() {
		
		assertFalse(dummy.findAirportByLongitude("-74.93").isEmpty());
	}

	@DisplayName("Should Throw IllegalArgumentException when longitude  provided  is blank/empty or null")
	@Test
	void testFindAirportByLongitudeBadParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByLongitude(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByLongitude("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when longitude does not have valid entry")
	@Test
	void testFindAirportByLongitudeBadParameterInDegrees() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByLongitude("abc");
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByLongitude("-181.0");
		});
	}
	
	@DisplayName("Should return List of Airports when a airport address is passed")
	@Test
	void testFindAirportByAirport() {
		assertFalse(dummy.findAirportByAddress("4281").size()<1);
	}

	@DisplayName("Should Throw IllegalArgumentException when airport address is blank/empty or null")
	@Test
	void testFindAirportByAirportBadParameter() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByAddress(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByAddress("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when Airport address is a string")
	@Test
	void testFindAirportByAirportBadParameterType() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByAddress(Mockito.anyString());
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByAddress(Mockito.anyString());
		});
	
	
}
	
	@DisplayName("Should return List of Airports when a small airport is searched")
	@Test
	void testSmallAirportByType() {
		assertFalse(dummy.findSmallAirportByType("small_airport").isEmpty());
	}

	@DisplayName("Should Throw IllegalArgumentException when airport searched By small Airport is blank/empty or null")
	@Test
	void testSmallAirportByTypeBadParameters() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findSmallAirportByType(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findSmallAirportByType("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when Airport By Type does not contain small airports")
	@Test
	void testSmallAirportByDifferentTypeBadParameters() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findSmallAirportByType("large_airport");
		});
		
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findSmallAirportByType("medium_airport");
		});
		
	}
	
	
	@DisplayName("Should return List of Airports when a searched By elevation height")
	@Test
	void testSmallAirportByElevationHeight() {
		assertFalse(dummy.findAirportByElevationFeet("11").isEmpty());
	}

	
	@DisplayName("Should Throw IllegalArgumentException when airport searched By elevation height is blank/empty or null")
	@Test
	void testSmallAirportByElevationFeet() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByElevationFeet(null);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByElevationFeet("");
		});

	}

	@DisplayName("Should throw IllegalArgumentException when Airport By elevation height  contains string")
	@Test
	void testSmallAirportByElevationFeetBadParameters() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByElevationFeet("abc");
		});
		
		
		
	}
	
	
	
	@DisplayName("Should return List of Airports when a searched By an airportId")
	@Test
	void testSmallAirportByAirportId() {
		assertFalse(dummy.findAirportByAirportId((long)6523).size()<0);
	}

	
	@DisplayName("Should Throw IllegalArgumentException when airport id is null")
	@Test
	void testAirportByAirportIdBadParam() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByAirportId(null);
		});
		

	}

	@DisplayName("Should throw IllegalArgumentException when negative Airport Id is passed ")
	@Test
	void testSmallAirportByAirportIdBadParameterType() {
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByAirportId((long) -123);
		});
		assertThrows(IllegalArgumentException.class, () -> {
			dummy.findAirportByAirportId((long) 0);
		});
		
		
	}
	
}