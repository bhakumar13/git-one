package com.airports.tests;

import com.sapient.airport.db.AirportsDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * As we want to query all the data in the database we should have the data loaded from the csv
 * As a precaution the same data must not be loaded multiple time
 */
@DisplayName("Airports Database UserStory Test Scenarios")
class AirportDBTest {
    AirportsDatabase database;

    @BeforeEach
    void init() {
        System.setProperty("db_file", "C:\\Users\\bhakumar13\\Documents\\airportCaseStudy\\airports.csv");
        database = AirportsDatabase.getInstance();
    }

    @DisplayName("Should not create 2 instances of the same database And Must return the same instance ")
    @Test
    void testSingletonDatabase() {
        AirportsDatabase expected = database;
        AirportsDatabase actual = AirportsDatabase.getInstance();
        assertSame(expected, actual);
    }

    @DisplayName("Should load database from a CSV File when initialized ")
    @Test
    void testDatabaseLoaded() {
        assertNotNull(database.getAirports());
        assertFalse(database.getAirports().isEmpty());
    }


    @DisplayName("Should throw RuntimeException when Database File Is missing")
    @Test
    void testDatabaseLoadMissingFile(){
        assertThrows(RuntimeException.class, ()->{
            System.setProperty("db_file", "C:\\Users\\bhakumar13\\Documents\\airportCaseStudy\\airports.csv");
            database = AirportsDatabase.getInstance();
        });
    }
}
