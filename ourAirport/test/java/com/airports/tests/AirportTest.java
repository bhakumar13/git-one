package com.airports.tests;

import static org.junit.jupiter.api.Assertions.*;

import com.sapient.airport.db.IAirportsDatabase;
import com.sapient.airport.model.Airport;
import com.sapient.airport.services.AirportService;
import com.sapient.airport.services.DummyAirportService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;

import static org.mockito.Mockito.*;

@DisplayName("Airport Test Scenarios")
public class AirportTest {
	IAirportsDatabase database;
	DummyAirportService service;

	@BeforeEach
	public void init() {
		database = Mockito.mock(IAirportsDatabase.class);
		service = Mockito.mock(DummyAirportService.class);
		List<Airport> list = null;
		list = Mockito.mock(List.class);
		when(list.size()).thenReturn(67486);
		when(database.getAirports()).thenReturn(list);
		when(database.getAirports().size()).thenReturn(67486);
	}

	@DisplayName("Should Return count of all airports")
	@Test
	void testCountAirports() {
		int actual = new AirportService(database).countAirports();
		int expected = 67486;
		assertEquals(expected, actual);
	}

//    
//    @DisplayName("Should Return Airport Details When Searched By Name ")
//    @Test
//    void testFindAirportByName() {
//        fail("Not yet Implemented ");
//    }
//
//    @DisplayName("Should Throw IllegalArgument Exception when Airport name is null or blank ")
//    @Test
//    void testFindAirportByNameBadParameter() {
//    }
//
//    @DisplayName("Should Throw IllegalArgument Exception when Airport code  is null or blank ")
//    @Test
//    void testFindAirportByCodeBadParameter() {
//        fail("Not yet Implemented ");
//    }
//
//    @DisplayName("Should return list of airports belonging to the same city  ")
//    @Test
//    void testFindAirportByCity() {
//        fail("Not Yet Implemented..");
//    }
//
//    @DisplayName("Should return list of airports belonging to the same Latitude  ")
//    @Test
//    void testFindAirportByLat() {
//        fail("Not Yet Implemented..");
//    }
//
//    @DisplayName("Should return list of airports belonging to the same longitude  ")
//    @Test
//    void testFindAirportByLongitude() {
//        fail("Not Yet Implemented..");
//    }
//
//    @DisplayName("Should throw IllegalArgument Exception if invalid argument /longitude is passed  ")
//    @Test
//    void testFindAirportByLongitudeBadParams() {
//        fail("Not Yet Implemented..");
//    }
//    // have written all the test cases
}
