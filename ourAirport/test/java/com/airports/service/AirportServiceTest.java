package com.airports.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.sapient.airport.constants.AirportSortConstant;
import com.sapient.airport.daos.IAirportDAO;
import com.sapient.airport.exception.AirportBadParameterException;
import com.sapient.airport.model.Airport;
import com.sapient.airport.services.IAirportService;
import com.sapient.airport.services.impl.AirportServiceImpl;

@DisplayName("Airport service Test Scenarios")
class AirportServiceTest {

	private IAirportDAO airportsDAO;

	private IAirportService airportService;

	private Airport airport;
	private List<Airport> airports;

	@BeforeEach
	public void init() {
		airportsDAO = Mockito.mock(IAirportDAO.class);
		airportService = new AirportServiceImpl(airportsDAO);
		airport = new Airport();
		airport.setName("Dubai");
		airport.setType("heliport");
		airport.setIdent("00A");
		airport.setLatitudeDegrees("40.07080078125");
		airport.setLongitudeDegrees("-74.9336013793945");
		airport.setGpsCode("KLAX");
		airport.setIataCode("LAX");
		airport.setLocalCode("LAX");
		airport.setHomeLink("http://www.wsdot.wa.gov/aviation/AllStateAirports/Colfax_LowerGraniteState.htm");
		airport.setWikipediaLink("https://en.wikipedia.org/wiki/McKenzie_Bridge_State_Airport");
		airport.setKeywords("00AR");
		airport.setContinent("US");
		airport.setElevationFeet("100");
		airport.setIsoCountry("US");
		airport.setScheduledService("no");
		airports = new ArrayList<>();
		airports.add(airport);
	}

	@DisplayName("Should returns list of airports when searched by airport name")
	@Test
	void testFindAirportByName() {
		when(airportsDAO.findAirportByName("Dubai", AirportSortConstant.ASE.toString())).thenReturn(airports);
		assertFalse(airportService.getAirportByName("Dubai", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should Throw AirportBadParameterException  when Airport name is null")

	@Test
	void testFindAirportByNameBadParameter() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByName(null, AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when Airport name is  blank ")

	@Test
	void testFindAirportByNameBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByName(" ", AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportNotfoundException when searched by airport name  not found")

	@Test
	void testFindAirportByNameNotFound() {
		assertTrue(airportService.getAirportByName("S", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns aiprort when searched by gpsCode")
	@Test
	void testFindAirportByGpsCode() {
		when(airportsDAO.findAirportByGpsCode("KLAX")).thenReturn(airport);
		assertNotNull(airportService.getAirportByGpsCode("KLAX"));

	}

	@DisplayName("Should Throw AirportBadParameterException  when gpsCode is null")
	@Test
	void testFindAirportByGpsCodeBadParameter() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByGpsCode(null));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when Iata Code is  blank ")
	@Test
	void testFindAirportByGpsCodeBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByGpsCode(" "));

	}

	@DisplayName("Should throw an aiport not found exception when gpscode is of length less than 3 ")
	@Test
	void testFindAirportByGpsCodeInvalid() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByGpsCode("k"));

	}

	@DisplayName("Should returns aiprort when searched by Iata Code")
	@Test
	void testFindAirportByIataCode() {
		when(airportsDAO.findAirportByIataCode("LAX")).thenReturn(airport);
		assertNotNull(airportService.getAirportByIataCode("LAX"));

	}

	@DisplayName("Should Throw AirportBadParameterException  when Iata Code is null")
	@Test
	void testFindAirportByIataCodeBadParameter() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByIataCode(null));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when IataCode is  blank ")
	@Test
	void testFindAirportByIataCodeBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByIataCode(" "));

	}

	@DisplayName("Should throw an aiport not found exception when iatacode is of length less than 3 ")
	@Test
	void testFindAirportByIataCodeInvalid() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByIataCode("K"));

	}

	@DisplayName("Should returns aiprort when searched by Local Code")
	@Test
	void testFindAirportByLocalCode() {
		when(airportsDAO.findAirportByLocalCode("LAX")).thenReturn(airport);
		assertNotNull(airportService.getAirportByLocalCode("LAX"));

	}

	@DisplayName("Should Throw AirportBadParameterException  when Local Code is null")
	@Test
	void testFindAirportByLocalCodeBadParameter() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByLocalCode(null));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when Local Code is  blank ")
	@Test
	void testFindAirportByLocalCodeBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByLocalCode(" "));

	}

	@DisplayName("Should throw an aiport not found exception when localcode is of length less than 3 ")
	@Test
	void testFindAirportByLocalCodeInvalid() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByLocalCode("K"));

	}

	@DisplayName("Should Return Airport Details When Searched By HomeLink ")
	@Test
	void testFindAirportByHomeLink() {
		when(airportsDAO.findAirportByHomeLink(
				"http://www.wsdot.wa.gov/aviation/AllStateAirports/Colfax_LowerGraniteState.htm"))
						.thenReturn((airport));
		assertNotNull(airportService.getAirportByHomeLink(
				"http://www.wsdot.wa.gov/aviation/AllStateAirports/Colfax_LowerGraniteState.htm"));
	}

	@DisplayName("Should Throw AirportBadParameterException when Airport HomeLink is null")
	@Test
	void testFindAirportByHomeLinkBadParameter() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByHomeLink(null));
	}

	@DisplayName("Should Throw IllegalArgument Exception when Airport HomeLink is blank ")
	@Test
	void testFindAirportByHomeLinkBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByHomeLink(" "));
	}

	@DisplayName("Should Return Airport Details When Searched By wikipediaLink ")
	@Test
	void testFindAirportByWikipediaLink() {
		when(airportsDAO.findAirportByWikipediaLink("https://en.wikipedia.org/wiki/McKenzie_Bridge_State_Airport"))
				.thenReturn(airport);
		assertNotNull(airportService
				.getAirportByWikipediaLink("https://en.wikipedia.org/wiki/McKenzie_Bridge_State_Airport"));
	}

	@DisplayName("Should Throw AirportBadParameterException when Airport wikipediaLink is null")
	@Test
	void testFindAirportByWikipediaLinkBadParameter() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByWikipediaLink(null));
	}

	@DisplayName("Should Throw IllegalArgument Exception when Airport wikipediaLink is blank ")
	@Test
	void testFindAirportByWikipediaLinkBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class, () -> airportService.getAirportByWikipediaLink(" "));
	}

	@DisplayName("Should Return Airport Details When Searched By keywords ")
	@Test
	void testFindAirportByKeywords() {
		when(airportsDAO.findAirportByKeywords("00AR", AirportSortConstant.ASE.toString())).thenReturn(airports);
		assertFalse(airportService.getAirportByKeyword("00AR", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should Throw AirportBadParameterException when Airport keywords is null")
	@Test
	void testFindAirportByKeywordsBadParameter() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByKeyword(null, AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw IllegalArgument Exception when Airport keywords is blank ")
	@Test
	void testFindAirportByKeywordsBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByKeyword(" ", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of airports when searched by Continent")
	@Test
	void testFindAirportByContinent() {
		when(airportsDAO.findByContinent("US", AirportSortConstant.ASE.toString())).thenReturn(airports);
		assertFalse(airportService.getAirportByContinent("US", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should Throw AirportBadParameterException  when Continent is null")
	@Test
	void testFindAirportByContinentBadParameter() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByContinent(null, AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when Continent is  blank ")
	@Test
	void testFindAirportByContinentBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByContinent(" ", AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should returns list of airports when searched by ElevationFeet")
	@Test
	void testFindAirportByElevationFeet() {
		when(airportsDAO.findByElevationFeet("100", AirportSortConstant.ASE.toString())).thenReturn(airports);
		assertFalse(airportService.getAirportByElevationFeet("100", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should Throw AirportBadParameterException  when ElevationFeet is null")
	@Test
	void testFindAirportByElevationFeetBadParameter() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByElevationFeet(null, AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when ElevationFeet is  blank ")
	@Test
	void testFindAirportByElevationFeetBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByElevationFeet(" ", AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportNotfoundException when searched by ElevationFeet  not found")
	@Test
	void testFindAirportByElevationFeetNotFound() {
		assertTrue(airportService.getAirportByElevationFeet("110", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns list of airports when searched by IsoCountry")
	@Test
	void testFindAirportByIsoCountry() {
		when(airportsDAO.findByIsoCountry("US", AirportSortConstant.ASE.toString())).thenReturn(airports);
		assertFalse(airportService.getAirportByIsoCountry("US", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should Throw AirportBadParameterException  when IsoCountry is null")
	@Test
	void testFindAirportByIsoCountryBadParameter() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByIsoCountry(null, AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when IsoCountry is  blank ")
	@Test
	void testFindAirportByIsoCountryBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByIsoCountry(" ", AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportNotfoundException when searched by IsoCountry  not found")
	@Test
	void testFindAirportByIsoCountryNotFound() {
		assertTrue(airportService.getAirportByIsoCountry("US", AirportSortConstant.ASE.toString()).isEmpty());
	}

	@DisplayName("Should returns aiprort when searched by isoRegion")
	@Test
	void testFindAirportByIsoRegion() {
		assertNotNull(airportService.getAirportByIsoRegion("US-PA", AirportSortConstant.ASE.toString()));

	}

	@DisplayName("Should Throw AirportBadParameterException  when isoRegion is null")
	@Test
	void testFindAirportByIsoRegionNull() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByIsoRegion(null, AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when isoRegion is  blank ")
	@Test
	void testFindAirportByIsoRegionBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByIsoRegion(" ", AirportSortConstant.ASE.toString()));

	}

	@DisplayName("Should returns aiprort when searched by Municipality")
	@Test
	void testFindAirportByMunicipality() {
		assertNotNull(airportService.getAirportByMunicipality("Bensalem", AirportSortConstant.ASE.toString()));

	}

	@DisplayName("Should Throw AirportBadParameterException  when Municipality is null")
	@Test
	void testFindAirportByMunicipalityNull() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByMunicipality(null, AirportSortConstant.ASE.toString()));
	}

	@DisplayName("Should Throw AirportBadParameterException Exception when Municipality is  blank ")
	@Test
	void testFindAirportByMunicipalityBadParameterWhenEmpty() {
		assertThrows(AirportBadParameterException.class,
				() -> airportService.getAirportByMunicipality(" ", AirportSortConstant.ASE.toString()));

	}


}
